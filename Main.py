from imutils.video import VideoStream
from imutils.video import FPS
import imutils
import time
import cv2
from LineDetector import LineDetector

detector = LineDetector()

# Initialize camera
# video_capture = cv2.VideoCapture(0)
# video_capture = cv2.VideoCapture('https://192.168.2.100:8080/video')
frame = cv2.imread('test.jpg')
# video_capture = VideoStream(usePiCamera=True).start()
time.sleep(2.0)
#fps = FPS().start()


while True:


    # CAPTURE FRAME-BY-FRAME
    # ret, frame = video_capture.read()
    # frame = video_capture.read()
    frame = imutils.resize(frame, 800)
    time.sleep(0.1)

    height, width, channels = frame.shape

    centerX = int(width/2)
    centerY = int(height/2)

    # Draw cicrcles in the center of the picture
    cv2.circle(frame, (centerX, 20), 20, (0, 0, 255), 1)
    cv2.circle(frame, (centerX, 20), 10, (0, 0, 255), 1)
    cv2.circle(frame, (centerX, 20), 2, (0, 0, 255), 2)

    outputFrame = detector.process(frame)

    cv2.imshow("line detect test", outputFrame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

