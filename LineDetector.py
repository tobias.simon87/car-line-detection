from collections import deque
import cv2
import numpy as np
import sys
import math
# import serial
import time

# Servo-GPIO (PWM-GPIO 18, Pin 12)
# servopin = 18

# GPIO initialisieren
# gpio.setmode(gpio.BCM)
# gpio.setup(servopin, gpio.OUT)

QUEUE_LENGTH = 50
KERNEL_SIZE = 7
LOW_THRESHOLD = 50
HIGH_THRESHOLD = 150

RHO = 1
THRESHOLD = 20
THETA = np.pi / 180
MIN_LINE_LENGTH = 20
MAX_LINE_GAP = 100

# Setze die Toleranz wo bei einer Abweichung gegengesteurt werden muss
TOLERANCE = 50


class LineDetector:

   # arduinoSerial = serial.Serial('/dev/ttyACM1', baudrate=9600)

    def __init__(self):
        self.left_lines = deque(maxlen=QUEUE_LENGTH)
        self.right_lines = deque(maxlen=QUEUE_LENGTH)

    def process(self, image):

        height, width, channels = image.shape

        black_mask = self.mask_color(image)
        # cv2.imshow("White Yellow Mask", black_mask)
        gray = self.convert_gray_scale(black_mask)
        cv2.imshow("Gray Mask", gray)
        smooth_gray = self.apply_smoothing(gray, KERNEL_SIZE)
        # cv2.imshow("Smooth Gray Mask", smooth_gray)
        edges = self.detect_edges(smooth_gray)
        cv2.imshow("edges Detection", edges)

        lines = self.hough_lines(edges)
        left_line, right_line = self.lane_lines(image, lines)

        def mean_line(line, lines):
            if line is not None:
                lines.append(line)

            if len(lines) > 0:
                line = np.mean(lines, axis=0, dtype=np.int32)
                line = tuple(map(tuple, line))  # make sure it's tuples not numpy array for cv2.line to work
            return line

        left_line = mean_line(left_line, self.left_lines)
        right_line = mean_line(right_line, self.right_lines)

        # Motor Steuerung

        # Berechne den Schnittpunkt der zwei Linien
        inter_section_point = self.calculateIntersectionPoint(left_line, right_line)
        # print("Intersection: %s" % inter_section_point.__str__())

        print(inter_section_point[0])

        # Berechne den Winkel der linken Linie
        left_line_angle = 180 - self.angle(left_line[0], left_line[1], (right_line[0][0], int(height)))

        # Berechne den Winkel der rechten Linie
        right_line_angle = self.angle(right_line[0], right_line[1], (left_line[0][0], int(height)))

        # Berechne en korrekturwinkel sodass ein Gleichschenkliges Dreieck entsteht
        correction_angle = abs(((left_line_angle + right_line_angle) / 2) - left_line_angle)

        # print("ALPHA %d" % left_line_angel)
        # print("BETA %d" % right_line_angel)
        print("Angel %d" % correction_angle)

        # Image Center x Axis
        center_x = width / 2

        # Wenn der Toleranzwert überschritten und der Schnittpunkt über der Mitte Liegt, muss nach Rechts gesteuert
        # werden
        if (inter_section_point[0] > center_x) & (abs(center_x - inter_section_point[0]) > TOLERANCE):
            cv2.putText(image, "DRIVE RIGHT %d GRAD" % correction_angle, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
            self.driveRight(correction_angle)
            # print("Drive Right %d Grad" % angle)

        # Wenn der Toleranzwert überschritten und der Schnittpunkt unter der Mitte Liegt, muss nach Links gesteuert
        # werden
        if (inter_section_point[0] < center_x) & (abs(center_x - inter_section_point[0]) > TOLERANCE):
            cv2.putText(image, "DRIVE LEFT %d GRAD" % correction_angle, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
            self.driveLeft(correction_angle)
            # print("Drive Left %d Grad" % angle)

        return self.draw_lane_lines(image, (left_line, right_line))

    def mask_color(self, image):
        # black color mask
        lower = np.uint8([0, 0, 0])
        upper = np.uint8([104, 104, 104])
        white_mask = cv2.inRange(image, lower, upper)
        return cv2.bitwise_and(image, image, white_mask)

    """ ---- Region of Interest Selection -------------------------------------------------------------------------- """

    @staticmethod
    def convert_gray_scale(image):
        return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

    """ ---- Gaussian Smoothing (Gaussian Blur) -------------------------------------------------------------------- """

    @staticmethod
    def apply_smoothing(image, kernel_size=15):
        """
        kernel_size must be postivie and odd
        """
        return cv2.GaussianBlur(image, (kernel_size, kernel_size), 0)

    """ ---- Edge Detection ---------------------------------------------------------------------------------------- """

    def detect_edges(self, image, low_threshold=LOW_THRESHOLD, high_threshold=HIGH_THRESHOLD):
        return cv2.Canny(image, low_threshold, high_threshold)

    """ ---- Region of Interest Selection -------------------------------------------------------------------------- """

    def select_region(self, image):
        """
        It keeps the region surrounded by the `vertices` (i.e. polygon).  Other area is set to 0 (black).
        """
        # first, define the polygon by vertices
        rows, cols = image.shape[:2]
        bottom_left = [cols * 0.1, rows * 0.95]
        top_left = [cols * 0.4, rows * 0.6]
        bottom_right = [cols * 0.9, rows * 0.95]
        top_right = [cols * 0.6, rows * 0.6]
        # the vertices are an array of polygons (i.e array of arrays) and the data type must be integer
        vertices = np.array([[bottom_left, top_left, top_right, bottom_right]], dtype=np.int32)
        return self.filter_region(image, vertices)

    @staticmethod
    def filter_region(image, vertices):
        """
        Create the mask using the vertices and apply it to the input image
        """
        mask = np.zeros_like(image)
        if len(mask.shape) == 2:
            cv2.fillPoly(mask, vertices, 255)
        else:
            cv2.fillPoly(mask, vertices,
                         (255,) * mask.shape[2])  # in case, the input image has a channel dimension
        return cv2.bitwise_and(image, mask)

    """ ---- Hough Transform Line Detection ------------------------------------------------------------------------ """

    @staticmethod
    def hough_lines(image):
        """
        `image` should be the output of a Canny transform.

        Returns hough lines (not the image with lines)
        """
        return cv2.HoughLinesP(image, rho=RHO, theta=THETA, threshold=THRESHOLD, minLineLength=MIN_LINE_LENGTH,
                               maxLineGap=MAX_LINE_GAP)

    def lane_lines(self, image, lines):
        left_lane, right_lane = self.average_slope_intercept(lines)

        y1 = image.shape[0]  # bottom of the image
        y2 = 0  # slightly lower than the middle

        # Eventuell hier der Einstiegspunkt für die Motorsteuerung

        left_line = self.make_line_points(y1, y2, left_lane)
        right_line = self.make_line_points(y1, y2, right_lane)

        return left_line, right_line

    @staticmethod
    def average_slope_intercept(lines):
        left_lines = []  # (slope, intercept)
        left_weights = []  # (length,)
        right_lines = []  # (slope, intercept)
        right_weights = []  # (length,)

        if lines is not None:
            for line in lines:
                for x1, y1, x2, y2 in line:
                    if x2 == x1:
                        continue  # ignore a vertical line
                    slope = (y2 - y1) / (x2 - x1)
                    intercept = y1 - slope * x1
                    length = np.sqrt((y2 - y1) ** 2 + (x2 - x1) ** 2)
                    if slope < 0:  # y is reversed in image
                        left_lines.append((slope, intercept))
                        left_weights.append((length))
                    else:
                        right_lines.append((slope, intercept))
                        right_weights.append((length))

        # add more weight to longer lines
        left_lane = np.dot(left_weights, left_lines) / np.sum(left_weights) if len(left_weights) > 0 else None
        right_lane = np.dot(right_weights, right_lines) / np.sum(right_weights) if len(right_weights) > 0 else None

        return left_lane, right_lane

    @staticmethod
    def make_line_points(y1, y2, line):
        """
        Convert a line represented in slope and intercept into pixel points
        """
        if line is None:
            return None

        slope, intercept = line

        # make sure everything is integer as cv2.line requires it
        x1 = int((y1 - intercept) / slope)
        x2 = int((y2 - intercept) / slope)
        y1 = int(y1)
        y2 = int(y2)

        # print("--------------------")
        # print("x1: "+x1.__str__())
        # print("y1: "+y1.__str__())
        # print("x2: "+x2.__str__())
        # print("y2: "+y2.__str__())
        # print("--------------------")

        return (x1, y1), (x2, y2)

    @staticmethod
    def draw_lane_lines(image, lines, color=[255, 0, 0], thickness=20):
        # make a separate image to draw lines and combine with the orignal later
        line_image = np.zeros_like(image)
        for line in lines:
            if line is not None:
                cv2.line(line_image, *line, color, thickness)
        # image1 * α + image2 * β + λ
        # image1 and image2 must be the same shape.
        return cv2.addWeighted(image, 1.0, line_image, 0.95, 0.0)

    @staticmethod
    def calculateIntersectionPoint(line1, line2):
        """
        Calculates the Intersection ov the two given Lines and returns the intersection Point
        """
        s1 = np.array(line1[0])
        e1 = np.array(line1[1])

        s2 = np.array(line2[0])
        e2 = np.array(line2[1])

        # Berechnung der Steigung
        a1 = (s1[1] - e1[1]) / (s1[0] - e1[0])

        # X
        b1 = s1[1] - (a1 * s1[0])

        a2 = (s2[1] - e2[1]) / (s2[0] - e2[0])
        b2 = s2[1] - (a2 * s2[0])

        if abs(a1 - a2) < sys.float_info.epsilon:
            return False

        x = (b2 - b1) / (a1 - a2)
        y = a1 * x + b1
        return (x, y)

    @staticmethod
    def angle(pt1, pt2, pt3):

        x1, y1 = pt1
        x2, y2 = pt2
        x3, y3 = pt3

        numerator = y2 * (x1 - x3) + y1 * (x3 - x2) + y3 * (x2 - x1)
        denominator = (x2 - x1) * (x1 - x3) + (y2 - y1) * (y1 - y3)
        ratio = numerator / denominator
        angleRad = math.atan(ratio)
        angleDeg = (angleRad * 180) / math.pi

        if angleDeg < 0:
            angleDeg = 180+angleDeg

        return angleDeg


    @staticmethod
    def driveLeft(correction_angel):

        # Servos Ansteuern
        # data = 100
        # self.arduinoSerial.write('100'.encode("ASCII"))

        value = (8.1 * (97 - correction_angel)) / 97
        print(value)

        return


    @staticmethod
    def driveRight(correction_angel):

        # Servos Ansteuern
        value = (8.1 * (97 + correction_angel)) / 97
        print(value)

        return